/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author Windows
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        //insert data
        try {
            String sql = "INSERT INTO Receipt (CustomerId,UserId,Total) VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println("Error to create receipt");
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //read data
        try {
            String sql = "SELECT r.Id as id,\n"
                    + "       Created,\n"
                    + "       CustomerId, \n"
                    + "       c.name as CustomerName,\n"
                    + "       c.tel as CustomerTel,\n"
                    + "       UserId,\n"
                    + "       u.name as UserName,\n"
                    + "       u.tel as UserTel,\n"
                    + "       Total\n"
                    + "  FROM Receipt r, Customer c, User u\n"
                    + "  WHERE r.CustomerId = c.Id AND r.UserId = U.Id"
                    + "  ORDER BY created DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("Id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Created")); //Throws exception
                int customerId = result.getInt("CustomerId");
                String customerName = result.getString("CustomerName");
                String customerTel = result.getString("CustomerTel");
                int userId = result.getInt("UserId");
                String userName = result.getString("UserName");
                String userTel = result.getString("UserTel");
                Double total = result.getDouble("Total");
                Receipt Receipt = new Receipt(id, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel));
                list.add(Receipt);
            }

        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt" + ex.getMessage());
        }

        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //read data
        try {
            String sql = "SELECT id,\n"
                    + "       created,\n"
                    + "       CustomerId, \n"
                    + "       c.name as CustomerName,\n"
                    + "       c.tel as CustomerTel,\n"
                    + "       UserId,\n"
                    + "       u.name as UserName,\n"
                    + "       u.tel as UserTel,\n"
                    + "       Total\n"
                    + "  FROM Receipt r, Customer c, User u\n"
                    + "  WHERE r.id = ? AND r.CustomerId = c.Id AND r.UserId = U.Id;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int rid = result.getInt("id");
                Date created = result.getDate("created");
                int customerId = result.getInt("CustomerId");
                String customerName = result.getString("CustomerName");
                String customerTel = result.getString("CustomerTel");
                int userId = result.getInt("UserId");
                String userName = result.getString("UserName");
                String userTel = result.getString("UserTel");
                Double total = result.getDouble("Total");
                Receipt Receipt = new Receipt(rid, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel));
                db.close();
                return Receipt;
            }

        } catch (SQLException ex) {
            System.out.println("Error : Unable to select receipt id " + id + "!!");
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        //insert data
        try {
            String sql = "DELETE FROM Receipt WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row: " + row);

        } catch (SQLException ex) {
            System.out.println("Error : Unable to delete receipt id " + id + "!!");
        }

        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        int row = 0;
//        //insert data
//        try {
//            String sql = "UPDATE Receipt SET Name = ?,Price = ? WHERE Id = ?";
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, object.getName());
//            stmt.setDouble(2, object.getPrice());
//            stmt.setInt(3, object.getId());
//            row = stmt.executeUpdate();
//        } catch (SQLException ex) {
//            Logger.getLogger(TestSelectReceipt.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        db.close();
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "Espresso", 50);
        Product p2 = new Product(2, "Green Tea", 40);
        User seller = new User(1, "Chakris Kulvitdamrongkul", "0123456789");
        Customer customer = new Customer(1, "Kara Yaimak", "0111111111");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        ReceiptDao dao = new ReceiptDao();
        System.out.println("id = " + dao.add(receipt));
        System.out.println(dao.getAll());
    }
}
